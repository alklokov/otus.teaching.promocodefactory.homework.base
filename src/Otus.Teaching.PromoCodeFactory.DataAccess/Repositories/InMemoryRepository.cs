﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T item)
        {
            item.Id = Guid.NewGuid();
            Data.Add(item);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T item)
        {
            Data[Data.FindIndex(i => i.Id == item.Id)] = item;
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            Data.RemoveAll(i => i.Id == id);
            return Task.CompletedTask;
        }
    }
}